import json
from datetime import datetime
from dataclasses import dataclass,asdict
from typing import List
import argparse

class DataUtil:
  @classmethod
  def create(cls, data):
    try:
      return cls(**data)
    except Exception as e:
      print(str(e))
      return None

  def typecheck(self):
    for (name, field_type) in self.__annotations__.items():
      if ('List' in str(field_type)):
        continue
      
      if not isinstance(self.__dict__[name], field_type):
        current_type = type(self.__dict__[name])
        raise TypeError(f"Error: `{field_type}  required for `{name}`, found `{current_type}` instead")

@dataclass
class Merchant(DataUtil):
  id: int
  name: str 
  distance: float

  def __post_init__(self):
    self.typecheck()

@dataclass 
class Offer(DataUtil):
  id: int
  title: str
  description: str
  category: int
  merchants: List[Merchant]
  valid_to: datetime

  def __post_init__(self):
    self.merchants = list(filter(lambda x: x is not None, map(lambda m: Merchant.create(m), self.merchants)))
    self.valid_to = datetime.strptime(self.valid_to, "%Y-%m-%d")
    self.typecheck()

@dataclass 
class Data(DataUtil):
  offers: List[Offer]
  def __post_init__(self):
    self.offers = list(filter(lambda o: o is not None and len(o.merchants) > 0, map(lambda o: Offer.create(o), self.offers)))


class MyJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d')
        return super().default(obj)

# filter1: +5 days
# filter2: closest merchant only -> discard others
# different categories, closest -> select 2
#multiple merchants, same distance -> choose 1!

class Solution:
  def __init__(self, to_pick=2, eligible_cats = {1,2,4}, infile='input.json'):
    self.to_pick = to_pick
    self.eligible_cats = eligible_cats
    self._load(infile)

  def _load(self, name):

    # load the file -> convert it into dicts
    with open(name) as fi:
      data = json.load(fi)
      self.offers = Data(**data).offers

  def get(self, date, outfile='output.json'):

    def filter_func(o : Offer):
      return o.category in self.eligible_cats and (o.valid_to - date).days >= 5

    valid_offers = filter(filter_func, self.offers)

    def map_func(o: Offer):
      o.merchants = [min(o.merchants, key=lambda m: m.distance)]
      return o
    valid_offers = map(map_func, valid_offers)
    min_by_cats = {}

    for o in valid_offers:
      cat = o.category
      if (cat not in min_by_cats):
        min_by_cats[cat] = o
      else:
        min_by_cats[cat] = min([min_by_cats[cat], o], key=lambda o: o.merchants[0].distance)

    min_by_cats = list(sorted(min_by_cats.values(), key=lambda o: o.merchants[0].distance))

    to_x = min(self.to_pick, len(min_by_cats))
    ans = min_by_cats[:to_x]
    self._write_solution_to_json(ans, outfile)

  def _write_solution_to_json(self, ans, outfile):
    with open(outfile, 'w') as fo:
      anslist = [asdict(o) for o in ans]

      ansdict = {'offers': anslist}
      fo.write(json.dumps(ansdict, indent=2, cls=MyJsonEncoder))

def parse_date(str):
    try:
      date = datetime.strptime(str, "%Y-%m-%d")
      return date
    except ValueError as e:
      print(str(e))
      return argparse.ArgumentTypeError("Can't parse date!")

def main():

  parser = argparse.ArgumentParser()
  parser.add_argument('date', nargs='?', type=parse_date, help='Input date in YYYY-MM-DD format', 
                      default=parse_date('2019-12-25'))

  args = parser.parse_args()

  s = Solution(infile='input.json')
  s.get(args.date, 'output.json')

if __name__=="__main__":
  main()