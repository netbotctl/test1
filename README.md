# Running the solution:

Requirements:

- Python 3.11


Command:
```
python3 main.py <date>
```

Example:

```
python3 main.py 2019-12-25
```

# Solution:

- Read the data from Json File. Discard any offer that can't be parsed, maybe due to incorrect format or typing, and discard offers without a valid merchant. 
- Filter the list of offers, keep only the offers that are available for more than 5 days from the check-in date.
- For each offer,  keep one closest merchant.
- For each category, keep one offer with closest merchant. After this, the number of offers is equal or less than the number of categories. 
- Sort the offers.
- Pick the 2 closest offers. Write the output.
